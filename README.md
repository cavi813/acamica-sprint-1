SPRINT 1 ACAMICA DELLILAH RESTO
En este proyecto se desarrolló una API, desde el backend para poder gestionar los pedidos un restaurante.
Los consumidores de la API van a poder realizar operaciones y gestiones sobre los usuarios, productos y pedidos.

REPOSITORIO: https://gitlab.com/cavi813/acamica-sprint-1.git
PUERTO: 3000

REQUISITOS PARA EL FUNCIONAMIENTO:
Instalar los siguientes paquetes en visual studio code (VSC), escribiendo el comando npm i "nombre del paquete" en la terminal 
    - express
    - nodemon
    - swagger-ui-express
    - swagger-jsdoc

Una vez instalados los paquetes se procede a copiar la carpeta del repositorio de la siguiente manera:
    1 - Escribimos en la terminal del VSC el comando "git init"
    2 - Escribimos en la terminal del VSC el comando "git clone https://gitlab.com/cavi813/acamica-sprint-1.git"

Una vez que tenemos la carpeta del repositorio copiada y los paquetes instalados, procedemos a utilizar el comando 
"npm run dev" para iniciar el nodemon y poder realizar las pruebas del programa.

Para probar las funciones del programa lo podemos hacer por Postman y/o Swagger
    POSTMAN: Podemos utilizar postman en su pagina oficial o descargar su aplicacion gratuita. En ella vamos a probar los GET, POST, PUT y DELETE, utilizando los siguientes link:
    - http://localhost:3000/listadousuarios 
    - http://localhost:3000/listadoproductos
    - http://localhost:3000/user
    - http://localhost:3000/login
    - http://localhost:3000/carrito
    - http://localhost:3000/finalizarPedido
    - http://localhost:3000/listadopedidos
    - http://localhost:3000/mediosdepago
    - http://localhost:3000/historialpedidos
    Indicando en cada uno la accion que queremos realizar (get, post, put. delete) y colocando en el body los paramentros que queremos ingresar (body, opcion "raw" tipo JSON)
        Ejemplo: ingresamos los parametros en el body con la siguiente estructura y presionar SEND.
                    {
                        "Parametro1": "Nombre1",
                        "Parametro2": "Nombre2"
                    }

    SWAGGER: Para utilizar el swagger copiamos el siguiente link en el buscador de nuestro navegador http://localhost:3000/api-docs/


AUTOR: Martinez Alvarez Camila
VERSION: 1.0.1