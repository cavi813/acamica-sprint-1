const { application, request, response } = require('express');
const express = require('express');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const server = express();

const swaggerOptions = {
    swaggerDefinition:{
      info: {
        title: "Sprint 1 Acamica", 
        description: "Sprint 1 Acamica",
        version: "1.0.0", 
        contact: {
          name: "Camila", 
        },
      },
    },
    apis:['./swagger.js'],
};

//4.configuracion de SwaggerDocs
const swaggerDocs = swaggerJSDoc(swaggerOptions);

//5.configuracion de endpoint de swagger

server.use('/api-docs', swaggerUI.serve,swaggerUI.setup(swaggerDocs));


//Configuracion variables de entorno
const env = require("./variables.entorno.json");
const environment = process.env.NODE_ENV || "development";
const port_env = env[environment];

console.log(port_env.port);

server.use(express.json());
server.use(express.urlencoded({extended: true}));


//Array usuarios

const datos = [
    {
        idUser: 1,
        nombre: "administrador",
        admin: true,
        email: "admin123@gmail.com",
        contrasenia: "123",
        status: false
        
},
];

const usuario = {
    idUser: 2,
    nombre: "Juan",
    apellido: "Perez",
    email: "juanperez97@hotmail.com",
    contrasenia:"123456",
    direccion: "elCano 23",
    telefono: "15654325",
    admin: false,
    status: false
    

  };
datos.push(usuario)
console.log(datos);


//MIDDLEWARE login

const userlogin = (req, res, next) =>{
    const {email, contrasenia} = req.body;
    let loginOk; 
    datos.forEach(elemento =>{
        if (elemento.email == email && elemento.contrasenia == contrasenia) {
            elemento.status = true 
            loginOk = elemento.status

        }
    }) 
    if(loginOk === true ){
        console.log(loginOk)
        next();
    } else{
        res.send("Datos erroneos")
    }
}
//MIDDLEWARE STATUS
const userStatus = (req, res, next) =>{
    let status
    datos.forEach(elemento=> {
        if (elemento.status) {
        status = elemento.status
            
        }    
    })
    if (status === true){
        next();
    } else{
        res.json({
            error: true,
            mensaje: "Usuario no logueado"
        })
    }
}
//MIDDLEWARE VERIFICACION DE ADMIN

const usuarioAdmin = (req, res, next)=>{
   let adminOk
    datos.forEach(elemento =>{
        if (elemento.status && elemento.admin == true) {
            adminOk = elemento.admin;
            console.log(adminOk)
            
        }
        
    })
    if (adminOk) {
        next();
        
    }else{
       res.send("Acceso solo para usuarios administradores")
    }
}

//MIDDLEWARE EMAIL DUPLICADO
const emailDuplicado = (req, res, next) =>{
    let duplicado = req.body.email;
    datos.forEach(elemento =>{
        if(duplicado == elemento.email){
            res.send("Email existente en el sistema")
        }
    }) 
    next();
}

//MIDDLEWARE EXISTENCIA DE PRODUCTO

const prodexistente = (req, res, next) =>{
    let productoOk = req.body.nombreProd;
    productos.forEach(elemento =>{
        if (productoOk !== elemento.nombreProd) {
            respuesta = {
                mensaje: "Producto no existente en el sistema"
            }
            
        } else{
            respuesta = {
             mensaje: "" 
             
            }
            next();
        }
    })
    res.send(respuesta)
    
}




// REGISTRO USUARIOS NUEVOS
server.post("/user",emailDuplicado, function(req, res){
    console.table(req.body);

if(!req.body.nombre || !req.body.apellido || !req.body.email || !req.body.telefono || !req.body.direccion || !req.body.contrasenia){
        respuesta = {
            error: true,
            mensaje: "Todos los campos son requeridos para completar el registro"

        };
    } else {
        if (req.body.nombre !== "" || req.body.apellido !== "" || req.body.email !== "" || req.body.telefono !== "" || req.body.direccion !== "" || req.body.contrasenia !== "" ){
            user = {
                nombre: req.body.nombre,
                apellido: req.body.apellido,
                email: req.body.email,
                contrasenia: req.body.contrasenia,
                telefono: req.body.telefono,
                direccion: req.body.direccion,
                idUser: parseInt(datos.length+1),
                admin: false,
                status: false,
                
            };
            respuesta = {
                error: false,
                mensaje: "Usuario creado correctamente",
                respuesta: user,
            };
            datos.push(user);
            console.log(datos);
        }
        
    }
    res.send(respuesta);
})



//Obtener datos de usuario 
server.get("/listadousuarios", userStatus, usuarioAdmin, (req, res)=>{
    res.send(datos)
});



server.post("/login", userlogin, userStatus, (req, res)=>{
    const {email, contrasenia} = req.body;
    let registrado = datos.findIndex(dato =>dato.email == email && dato.contrasenia == contrasenia)
        if (registrado >=0 ){
            respuesta = {
                mensaje: 'Usuario correcto, Bienvenido',
                listado: productos
            }
            
        }else{
            respuesta = {
                mensaje: 'Datos ingresados erroneos'
            }  
        }
        res.json(respuesta)
    
}
)

const pedidos = [
    { idpedido: 1,
      mediosPago: "Debito",
      nombreProd: "Sandwich veggie",
      cantidad1: 3,
      precio2: 310,
      total: 620,
      estado: "pendiente",
      fecha:"Sat Jul 31 2021 23:55:01",
      emailUser: "juanperez97@hotmail.com",
      direccionEnvio: "Calle 13"

}];






//Medios de pago

const mediosPago = [
    {
        id: 1,
        medio: 'Efectivo'
    },
    {
        id: 2,
        medio: "Debito"
    },
    {
        id: 3,
        medio: "Credito"
    }
]

//ADMINISTRADORES PUEDAN VER MEDIOS DE PAGO 
server.get("/mediosdepago",userStatus, usuarioAdmin,  (req,res)=>{
    res.send(mediosPago);
});




//ADMINISTRADORES PUEDAN CREAR NUEVOS MEDIOS DE PAGO 

server.post("/mediosdepago", userStatus, usuarioAdmin, (req, res)=>{
    
    if(!req.body.medio){
        respuesta = {
            error: true,
            mensaje: "Todos los campos son requeridos para crear medio de pago"
        };
    } else {
        if (req.body.medio !== "" ){
            nuevoMedio = {
                id: parseInt(mediosPago.length+1),
                medio: req.body.medio,
                
            };
            respuesta = {
                error: false,
                mensaje: "Medio de pago creado correctamente",
                
            };
            mediosPago.push(nuevoMedio);
            console.log(nuevoMedio);
        }
        
    }
    res.send(respuesta);
})


//ADMINISTRADORES PUEDAN ELIMINAR UN MEDIO DE PAGO

server.delete("/mediosdepago", userStatus, usuarioAdmin, (req, res)=>{
    
    const {medio} = req.body

    if(!req.body.medio){
        respuesta = {
            mensaje: "El medio es requerido",
        };
    }else {
        if (req.body.medio !== "") {
            let medio1 = req.body.medio;
            mediosPago.forEach(function(x,index, object){
                if (x.medio == medio1) {
                    object.splice(index, 1);
                    
                }
            }
        )
        }
        respuesta = {
            mensaje: "Medio de pago borrado",
            respuesta: mediosPago
            
        }
    }
    res.send(respuesta)
})



//ADMINISTRADORES PUEDAN EDITAR MEDIOS DE PAGO

server.put("/mediosdepago", userStatus, usuarioAdmin, (req, res) =>{
    if (!req.body.medio || !req.body.id) {
        respuesta = {
            error: true,
            mensaje: "Todos los datos son requeridos",
        }
    } else{
        if (req.body.id !== "" || req.body.medio !== "") {
            let idReq = req.body.id;
            let medioReq = req.body.medio;
            
            mediosPago.map((item)=> {
                if (item.id == idReq) {
                    item.medio = medioReq;
                    
                }
            });
            respuesta = {
                error: false,
                mensaje: "Medio de pago actualizado correctamente"
            }
        }
    }
    res.send(respuesta);
})



//PRODUCTOS
const productos = [
    {
    idprod:1,
    nombreProd: 'Bagel de salmon',
    precio: 425
},
{
    idprod: 2,
    nombreProd: 'Hamburguesa clasica',
    precio: 350
},
{
    idprod: 3,
    nombreProd: 'Sandwich veggie',
    precio: 310
},
{
    idprod: 4,
    nombreProd: 'Ensalada veggie',
    precio: 340
},
{
    idprod: 5,
    nombreProd: 'Focaccia',
    precio: 330
},
{
    idprod: 6,
    nombreProd: 'Sandwich focaccia',
    precio: 440
}
];
 

//LISTADO DE PRODUCTOS

server.get("/listadoproductos", (req, res)=>{
    res.send(productos)
});


//NUEVOS PRODUCTOS
server.post("/listadoproductos", userStatus, usuarioAdmin, (req, res)=>{
    
    if(!req.body.nombreProd || !req.body.precio){
        respuesta = {
            error: true,
            mensaje: "Todos los campos son requeridos para agregar un producto"
        };
    } else {
        if (req.body.nombreProd!== "" || req.body.precio!== "" ){
            nuevoProd = {
                idprod: parseInt(productos.length+1),
                nombreProd: req.body.nombreProd,
                precio: req.body.precio
                
            };
            respuesta = {
                error: false,
                mensaje: "Producto agregado correctamente",
                
            };
            productos.push(nuevoProd);
            console.log(nuevoProd);
        }
        
    }
    res.send(respuesta);
})


//ADMINISTRADORES PUEDAN EDITAR PRODUCTOS

server.put("/listadoproductos", userStatus, usuarioAdmin, (req, res) =>{
    if (!req.body.idprod || !req.body.nombreProd || !req.body.precio) {
        respuesta = {
            error: true,
            mensaje: "Todos los datos son requeridos",
        }
    } else{
        if (req.body.idprod !== "" || req.body.nombreProd !== "" || req.body.precio !== "") {
            let id1 = req.body.idprod;
            let prod1 = req.body.nombreProd;
            let precio1 = req.body.precio;
            
            productos.map((item1)=> {
                if (item1.idprod === id1) {
                  item1.nombreProd = prod1
                  item1.precio = precio1
                    
                    
                }
            });
            respuesta = {
                error: false,
                mensaje: "Producto actualizado correctamente",
                respuesta:(productos)
            }
        }
    }
    res.send(respuesta);
})

//ADMINISTRADORES PUEDAN BORRAR UN PRODUCTO

server.delete("/listadoproductos", userStatus, usuarioAdmin, (req, res)=>{
    
    const {idprod} = req.body

    if(!req.body.idprod){
        respuesta = {
            mensaje: "El id del producto es requerido",
        };
    }else {
        if (req.body.idprod !== "") {
            let idborrar = req.body.idprod;
            productos.forEach(function(x,index, object){
                if (x.idprod == idborrar) {
                    object.splice(index, 1);
                    
                }
            }
        )
        }
        respuesta = {
            mensaje: "Producto borrado",
            respuesta: productos
            
        }
    }
    res.send(respuesta)
})

//ESTADOS DE PEDIDOS

const estadopedido = [
    {estado: "pendiente" },
    {estado: "confirmado"},
    {estado: "en preparacion"},
    {estado: "enviado"},
    {estado: "entregado"}

]

const carrito = []


server.post("/carrito", userStatus, prodexistente, (req, res)=>{

    const{nombreProd, cantidad} = req.body;
    let buscarPrecio = productos.findIndex(x=> x.nombreProd == req.body.nombreProd);
    let precioXunidad
    let cantidad1 = cantidad
   if(!req.body.nombreProd){
    respuesta = {
        error: true,
        mensaje: "Todos los campos son requeridos para realizar el pedido"
    };
} else {
    if (req.body.cantidad!== "" || req.body.nombreProd!==""){
        descripcionPedido = {
            idcarrito: parseInt(carrito.length+1),
            nombreProd: req.body.nombreProd,
            cantidad: req.body.cantidad,
            precioXunidad: productos[buscarPrecio].precio,
            subtotal: parseFloat(productos[buscarPrecio].precio * cantidad)
            
        }
        respuesta = {
            mensaje: "Productos agregados al carrito correctamente",
            respuesta: descripcionPedido
            
        };
        carrito.push(descripcionPedido);
        console.log(descripcionPedido);
        console.log(req.body)
    }
    
}
res.send(respuesta);
})
    


server.post("/finalizarPedido", userStatus, (req, res)=>{
    let pedidoUser = datos.findIndex( x => x.status == true);
    let direccionEnvio
    let descripcion 
    if( !req.body.mediosPago){
        respuesta = {
            error: true,
            mensaje: "Todos los campos son requeridos para realizar el pedido"
        };
    } else {
        if (req.body.mediosPago !== "" || req.body.direccionEnvio!=="" || req.body.cantidad!== "" || req.body.nombreProd!==""){
            nuevoPedido = {
                idpedido: parseInt(pedidos.length+1),
                estado: "pendiente",
                fecha: Date(),
                carrito1: carrito, 
                emailUser: datos[pedidoUser].email, 
                direccionEnvio: req.body.direccionEnvio,   
            };
            
            respuesta = {
                mensaje: "Pedido creado correctamente",
                respuesta: nuevoPedido
                
            };
            pedidos.push(nuevoPedido);
            console.log(nuevoPedido);
            console.log(req.body)
        }
        
    }
    res.send(respuesta);
})



//HISTORIAL DE PEDIDOS
server.get("/historialpedidos", (req,res)=>{
    let email1 = datos.findIndex( x => x.status === true);
    let buscarEmail = datos[email1].email
    pedidos.forEach(elemento =>{
        if (buscarEmail == elemento.emailUser) {
            res.send(pedidos)
            
        }else{
            res.send("No tiene pedidos registrados")
        }
    })
    
})


//MOSTRAR PEDIDOS DE USUARIOS

server.get("/listadopedidos", userStatus, usuarioAdmin,  (req, res)=>{
    res.send(pedidos);
})


//ADMINISTRADORES PUEDAN EDITAR ESTADO DE PEDIDO
server.put("/listadopedidos", userStatus, usuarioAdmin, (req, res) =>{
    const{idpedido, estaodo} = req.body;
    if (!req.body.idpedido || !req.body.estado ) {
        respuesta = {
            error: true,
            mensaje: "Todos los datos son requeridos",
        }
    } else{
        if (req.body.idpedido !== "" || req.body.estado !== "" ) {
            let id2 = req.body.idpedido;
            let estado1 = req.body.estado;
            
            pedidos.forEach((item2)=> {
                if (item2.idpedido === id2) {
                  item2.estado = estado1   
                    
                }
            });
            respuesta = {
                error: false,
                mensaje: "Estado de pedido actualizado",
                respuesta: (pedidos)
            }
        }
    }
    res.send(respuesta);
})




//configuracion listen servidor

server.listen(port_env.port, function(){
    console.log("Servidor corriendo por el puerto: " + port_env.port);
})

