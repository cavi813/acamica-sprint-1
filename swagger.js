/**
 * @swagger
 * /user:
 *  post:
 *    description: Registrar un nuevo usuario
 *    parameters:
 *    - name: nombre
 *      description: Nombre del usuario
 *      in: formData
 *      type: string
 *    - name: apellido
 *      description: Apellido del usuario
 *      in: formData
 *      type: string
 *    - name: email
 *      description: email del usuario
 *      in: formData
 *      type: string
 *    - name: contrasenia
 *      description: contrasenia del usuario
 *      in: formData
 *      type: string
 *    - name: telefono
 *      description: telefono del usuario
 *      in: formData
 *      type: string
 *    - name: direccion
 *      description: Direccion del usuario
 *      in: formData
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /listadousuarios:
 *  get:
 *    description: retorna listado de usuarios registrados
 *    responses:
 *      200:
 *       description: Success
 */

/**
 * @swagger
 * /login:
 *  post:
 *    description: Registrar un nuevo usuario
 *    parameters:
 *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      type: string
 *    - name: contrasenia
 *      description: Contraseña del usuario
 *      in: formData
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /mediosdepago:
 *  get:
 *    description: Retorna listado de medios de pago a los administradores
 *    responses:
 *      200:
 *       description: Success
 */

/**
 * @swagger
 * /mediosdepago:
 *  post:
 *    description: Agregar un nuevo medio de pago
 *    parameters:
 *    - name: medio
 *      description: Medio de pago nuevo
 *      in: formData
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /mediosdepago:
 *  delete:
 *    description: Eliminar un nuevo usuario
 *    parameters:
 *    - name: medio
 *      description: Medio de pago a borrar
 *      in: formData
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /carrito:
 *  post:
 *    description: Agregar pedidos al carrito 
 *    parameters:
 *    - name: nombreProd
 *      description: Hamburguesa clasica, Sandwich veggie, Focaccia, Ensalada veggie, Sandwich focaccia, Bagel de salmon
 *      in: formData
 *      type: string
 *    - name: cantidad
 *      description: cantidad del producto
 *      in: formData
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /mediosdepago:
 *  put:
 *    description: Editar un medio de pago actual
 *    parameters:
 *    - name: medio
 *      description: medio de pago
 *      in: formData
 *      type: string
 *    - name: id
 *      description: id del medio de pago
 *      in: formData
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */
/**
 * @swagger
 * /listadoproductos:
 *  get:
 *    description: Retorna el listado de productos 
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /listadoproductos:
 *  post:
 *    description: Agregar un nuevo producto
 *    parameters:
 *    - name: nombreProd
 *      description: Nombre del producto
 *      in: formData
 *      type: string
 *    - name: precio
 *      description: Precio del producto
 *      in: formData
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */
/**
 * @swagger
 * /listadoproductos:
 *  put:
 *    description: Editar un producto
 *    parameters:
 *    - name: idprod
 *      description: id del producto
 *      in: formData
 *      type: string
 *    - name: nombreProd
 *      description: Nombre del producto
 *      in: formData
 *      type: string
 *    - name: precio
 *      description: Precio del producto
 *      in: formData
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */


/**
 * @swagger
 * /listadoproductos:
 *  delete:
 *    description: Borrar producto por ID
 *    parameters:
 *    - name: idprod 
 *      description: ID del producto
 *      in: formData
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /finalizarPedido:
 *  post:
 *    description: Realizar un pedido
 *    parameters:
 *    - name: mediosPago
 *      description: Medio de pago (Debito, Efectivo, Credito)
 *      in: formData
 *      type: string
 *    - name: direccionEnvio
 *      description: Direccion de la entrega opcional
 *      in: formData
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /historialpedidos:
 *  get:
 *    description: Retorna el historial de pedidos de cada persona
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /listadopedidos:
 *  get:
 *    description: retorna listado de pedidos a los administradores
 *    responses:
 *      200:
 *       description: Success
 */

/**
 * @swagger
 * /listadopedidos:
 *  put:
 *    description: Editar un pedido
 *    parameters:
 *    - name: idpedido
 *      description: id del pedido
 *      in: formData
 *      type: string
 *    - name: estado
 *      description: estado del pedido
 *      in: formData
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */



